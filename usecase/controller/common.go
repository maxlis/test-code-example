package controller

import (
	"context"
	"github.com/pkg/errors"
)

//check if everything going ok
func (s *service) Health(_ctx context.Context) error {
	ctx, cancel := context.WithTimeout(_ctx, s.timeout)
	defer cancel()

	if err := s.repo.Health(ctx); err != nil {
		return errors.WithMessage(err, "database health check")
	}

	return nil
}

//Find returns the smallest index i at which x == a[i],
//or len(a) if there is no such index.
func (s *service) FindID(id uint32, sl []uint32) int {
	for i, n := range sl {
		if id == n {
			return i
		}
	}
	return len(sl)
}
