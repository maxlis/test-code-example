package controller

import (
	"context"
	"encoding/json"
	"github.com/go-pg/pg/v9"
	"gitlab.com/max.lis/test-code-example/model"
	"io/ioutil"
	"os"
	"time"
)

func (s service) GetDiscountData(_ctx context.Context, phone string) (*model.Discount, error) {
	ctx, cancel := context.WithTimeout(_ctx, s.timeout)
	defer cancel()

	res := &model.Discount{}

	if err := s.repo.GetWhere(ctx, res, "phone LIKE ?", phone); err != nil {
		return res, err
	}

	res.ToDTO()

	return res, nil
}

func (s service) UpdateDiscountData(_ctx context.Context) (*model.Discounts, error) {
	ctx, cancel := context.WithTimeout(_ctx, s.timeout)
	defer cancel()

	res := &model.Discounts{}

	data, err := os.Open("db/Bonus.txt")
	defer data.Close()

	if err != nil {
		return nil, err
	}

	st, _ := data.Stat()
	if st.ModTime().Unix()+86400 <= time.Now().Unix() {
		return res, nil
	}

	byteValue, _ := ioutil.ReadAll(data)

	err = json.Unmarshal(byteValue, res)
	if err != nil {
		return nil, err
	}

	allIDs := make([]uint32, 0)
	for _, discount := range *res {
		allIDs = append(allIDs, discount.ID)
	}

	old := &model.Discounts{}
	_ = s.repo.GetWhere(ctx, old, "id IN (?)", pg.In(allIDs))

	oldIDs := make([]uint32, 0)
	for _, discount := range *old {
		oldIDs = append(oldIDs, discount.ID)
	}

	in := model.Discounts{}

	for _, discount := range *res {
		discount.FromDTO()

		switch s.FindID(discount.ID, oldIDs) {
		case len(oldIDs):
			in = append(in, discount)
		default:
			_ = s.repo.UpdateWhere(ctx, &discount, "id = ?", discount.ID)
		}
	}

	if len(in) > 0 {
		_ = s.repo.Insert(ctx, &in)
	}

	return res, nil
}
