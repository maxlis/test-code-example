package controller

import (
	"gitlab.com/max.lis/test-code-example/config"
	"gitlab.com/max.lis/test-code-example/repository"
	"gitlab.com/max.lis/test-code-example/usecase"
	"time"
)

type service struct {
	repo    repository.GoPG
	timeout time.Duration
}

func New(r repository.GoPG, cnf *config.Usecase) usecase.Interface {
	return &service{repo: r, timeout: cnf.Timeout}
}
