//go:generate mockery -all
package usecase

import (
	"context"
	"gitlab.com/max.lis/test-code-example/model"
)

type Interface interface {
	Health(_ctx context.Context) error

	GetDiscountData(_ctx context.Context, phone string) (*model.Discount, error)
	UpdateDiscountData(_ctx context.Context) (*model.Discounts, error)
}
