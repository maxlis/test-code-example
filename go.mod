module gitlab.com/max.lis/test-code-example

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-pg/pg/v9 v9.1.6
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/lib/pq v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.18.0
	github.com/rubenv/sql-migrate v0.0.0-20200402132117-435005d389bc
	github.com/spf13/viper v1.6.3
)
