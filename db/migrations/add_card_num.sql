-- +migrate Up
ALTER TABLE discounts ADD card_num varchar(32);

-- +migrate Down
ALTER TABLE discounts DROP card_num;