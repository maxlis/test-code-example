-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE discounts (id bigint PRIMARY KEY, name varchar(255) NOT NULL, phone varchar(15), bonus numeric(7,2), birthday date);


-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE discounts;