-- +migrate Up
ALTER TABLE discounts ADD last_update date;

-- +migrate Down
ALTER TABLE discounts DROP last_update;