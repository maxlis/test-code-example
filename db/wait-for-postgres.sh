#!/bin/sh
# wait-for-postgres.sh

set -e

dbname="$1"
shift
cmd="$@"

until psql -h "db" -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

until $(psql -h "db" -U "postgres" -lqt | cut -d \| -f 1 | grep -wq "$dbname"); do
  >&2 echo "Database does not exist"

  >&2 echo "Creata database"
  psql -h "db" -U "postgres" -c "CREATE DATABASE "$dbname""
  psql -h "db" -U "postgres" -c "GRANT ALL PRIVILEGES ON DATABASE "$dbname" TO postgres;"

  sleep 1
done

>&2 echo "Postgres is up - executing command"
exec $cmd