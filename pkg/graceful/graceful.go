package graceful

import (
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	defaultWaitSec = 2
)

type Queue struct {
	fifo []func()
}

// New instance of graceful shutdfown.
func New() *Queue {
	return &Queue{fifo: make([]func(), 0, 1)}
}

// Add to graceful shutdown queue task
func (q *Queue) Add(fn func()) {
	q.fifo = append(q.fifo, fn)
}

// Shutdown lock main thread and wait signal for stop app.
// also perform some queued tasks before stop in specific order.
func (q *Queue) Shutdown() {
	var gracefulStop = make(chan os.Signal, 1)

	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	signal.Notify(gracefulStop, syscall.SIGILL)

	_ = <-gracefulStop
	//TODO log here

	for _, cb := range q.fifo {
		//TODO log here
		cb()
		//TODO log here
		<-time.After(defaultWaitSec * time.Second)
	}

	time.Sleep(defaultWaitSec * time.Second)
	//TODO log here
	os.Exit(0)
}
