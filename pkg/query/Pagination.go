package query

import (
	"math"
	"net/url"
	"strconv"
)

const ItemsPerPage = 25

type Pagination struct {
	CurrentPage int `json:"current_page"`
	ItemPerPage int `json:"item_per_page"`
	TotalPages  int `json:"total_pages"`
	TotalCount  int `json:"total_count"`
}

func (v *Pagination) InitPagination(q url.Values) {
	v.CurrentPage, _ = strconv.Atoi(q.Get("page"))
	if v.CurrentPage == 0 {
		v.CurrentPage = 1
	}

	v.ItemPerPage, _ = strconv.Atoi(q.Get("ipp"))
	if v.ItemPerPage == 0 {
		v.ItemPerPage = ItemsPerPage
	}
}

func (v *Pagination) GetTotalPages() {
	v.TotalPages = int(math.Ceil(float64(v.TotalCount / v.ItemPerPage)))
}

func (v *Pagination) GetOffset() int {
	return (v.CurrentPage - 1) * v.ItemPerPage
}
