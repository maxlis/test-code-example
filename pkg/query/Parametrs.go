package query

import (
	"net/url"
	"strconv"
)

type Query struct {
	Pagination Pagination
	Filter     map[string]string
}

func (v *Query) InitQuery(q url.Values) {
	v.Pagination.InitPagination(q)

	v.Filter = make(map[string]string)
	v.Filter["pa"] = q.Get("pa")
	v.Filter["search"] = q.Get("search")
}

func (v *Query) ToInt(idx string) int {
	var res = 0

	if v.Filter[idx] != "" {
		res, _ = strconv.Atoi(v.Filter[idx])
	}

	return res
}
