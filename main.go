package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/max.lis/test-code-example/config"
	"gitlab.com/max.lis/test-code-example/delivery/handlers"
	"gitlab.com/max.lis/test-code-example/pkg/graceful"
	gopg "gitlab.com/max.lis/test-code-example/repository/go-pg"
	"gitlab.com/max.lis/test-code-example/usecase/controller"
	"os"
	"strings"
)

type Test struct {
	ID    uint32
	Title string
}

func main() {
	config.Initialize()

	if migration(config.Cfg.Database) {
		return
	}

	db := gopg.New(config.Cfg.Service, config.PG(config.Cfg.Database), false)
	uc := controller.New(db, config.Cfg.Usecase)

	h := handlers.New(uc)
	go h.Serve(config.Cfg.Delivery.HTTP.Addr)

	grace := graceful.New()
	grace.Add(db.Stop)
	grace.Add(h.Stop)
	grace.Shutdown()
}

func migration(cfg *config.Database) bool {
	if len(os.Args) <= 1 {
		return false
	}

	switch strings.ToLower(os.Args[1]) {
	case "init":
		//database connection params
		p := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=%s", cfg.Host, cfg.Port, cfg.User, cfg.DatabaseName, cfg.SSLMode)

		//path to migration files
		m := &migrate.FileMigrationSource{Dir: "db/migrations"}

		//database connection
		db, err := sql.Open("postgres", p)
		if err != nil {
			fmt.Println(err.Error()) //TODO log here
			return true
		}
		defer db.Close()

		//check migrations
		n, err := migrate.Exec(db, "postgres", m, migrate.Up)
		if err != nil {
			fmt.Println(err.Error()) //TODO log here
			return true
		}
		fmt.Printf("Applied %d migrations!\n", n)

		return false
	default:
		return false
	}
}
