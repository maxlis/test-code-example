package config

import (
	gopg "gitlab.com/max.lis/test-code-example/repository/go-pg"
)

func PG(database *Database) *gopg.Config {
	return &gopg.Config{
		Type:         database.Type,
		Host:         database.Host,
		Port:         database.Port,
		User:         database.User,
		Password:     database.Password,
		SSLMode:      database.SSLMode,
		SSLRootCert:  database.SSLRootCert,
		SSLCert:      database.SSLCert,
		SSLKey:       database.SSLKey,
		DatabaseName: database.DatabaseName,
		MaxIdleConns: database.MaxIdleConns,
		MaxOpenConns: database.MaxOpenConns,
		Direct:       database.Direct,
	}
}
