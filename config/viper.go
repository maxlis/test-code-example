package config

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/joho/godotenv"
	"github.com/pkg/errors"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

var (
	Cfg      *Config
	PathList = []string{"./.env", "../.env", "../../.env", "../../../.env", "../../../../.env", "../../../../../.env"}
)

//ViperInitialize initialize service conf
func Initialize() {
	//.env file read
	for _, p := range PathList {
		_ = godotenv.Load(p)
	}

	bindDefaults()

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	cfg, err := getCfg()
	if err != nil {
		log.Fatal().Err(err).Msg("get config")
	}

	Cfg = cfg

	if err = checkCfg(); err != nil {
		log.Fatal().Err(err).Msg("check config")
	}

	if IsDev() {
		log.Info().Interface("cfg", cfg).Msg("config")
	}
}

//GetCfg return service config
func getCfg() (*Config, error) {
	if !viper.IsSet("Database") {
		return nil, fmt.Errorf("miss service configuration:'Database' key")
	}

	cfg := &Config{}
	err := viper.Unmarshal(cfg)

	if err != nil {
		return nil, errors.Wrap(err, "wrong service configuration")
	}

	return cfg, nil
}

//CheckCfg check config on errors
func checkCfg() error {
	switch viper.Get(GoEnvKey) {
	case DevEnv, ProdEnv:
	default:
		return fmt.Errorf("GO_ENV not valid: %s", viper.Get(GoEnvKey))
	}

	switch Cfg.Database.SSLMode {
	case "disable", "require", "verify-ca", "verify-full":
	default:
		return fmt.Errorf("SSLMode not valid: %s", Cfg.Database.SSLMode)
	}

	log.Debug().Str(GoEnvKey, viper.GetString(GoEnvKey)).Msg("hello")

	return nil
}

func bindDefaults() {
	viper.SetDefault(GoEnvKey, ProdEnv)

	// not underscore, here dots.
	viper.SetDefault("Database.Type", "postgres")
	viper.SetDefault("Database.SSLMode", "disable")
	viper.SetDefault("Delivery.HTTP.Addr", ":8080")
	viper.SetDefault("Delivery.GRPC.Addr", ":9111")
	viper.SetDefault("Database.MaxIdleConns", "10")
	viper.SetDefault("Database.MaxOpenConns", "10")
	viper.SetDefault("Usecase.Timeout", "5s")

	b := make([]byte, 10)
	if _, err := rand.Read(b); err != nil {
		log.Fatal().Err(err).Msg("read")
	}

	viper.SetDefault("Usecase.Default.Administrator.Password", hex.EncodeToString(b))

	for _, v := range []string{"Service", "Delivery.HTTP.Addr",
		"Database.Host", "Database.Port", "Database.User", "Database.Password",
		"Database.DatabaseName", "Database.MaxIdleConns", "Database.MaxOpenConns",
		"Database.SSLMode", "Database.SSLRootCert", "Database.SSLCert", "Database.SSLKey", "Database.SSLCert",
		"Database.Direct", GoEnvKey, "Usecase.Timeout",
	} {
		if err := viper.BindEnv(v); err != nil {
			log.Error().Err(err).Msg("config.bind_default")
		}
	}
}
