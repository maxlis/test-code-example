package config

import (
	"time"

	"github.com/spf13/viper"
)

const (
	DevEnv  = "development"
	ProdEnv = "production"

	//enum: development, test, production
	GoEnvKey = "GO_ENV"
)

//Config stores all configuration options
type Config struct {
	Service  string
	Database *Database
	Delivery *Delivery
	Usecase  *Usecase
}

//Database stores sdb connection options
type Database struct {
	Type string

	//The host to connect to. Values that start with / are for unix domain sockets. (default is localhost)
	Host string

	//The port to bind to.
	Port string

	//The user to sign in as
	User string

	//The user's password
	Password string

	// * disable - No SSL
	// * require - Always SSL (skip verification)
	// * verify-ca - Always SSL (verify that the certificate presented by the server was signed by a trusted CA)
	// * verify-full - Always SSL (verify that the certification presented by
	//  the server was signed by a trusted CA and the server host name matches the one in the certificate)
	SSLMode string

	//The location of the root certificate file. The file must contain PEM encoded data.
	SSLRootCert string

	//Cert file location. The file must contain PEM encoded data.
	SSLCert string

	//Key file location. The file must contain PEM encoded data.
	SSLKey string

	//The name of the sdb to connect to
	DatabaseName string

	MaxIdleConns int

	MaxOpenConns int

	//Direct db pg connection string
	//https://godoc.org/github.com/lib/pq#hdr-Connection_String_Parameters
	Direct string
}

type Delivery struct {
	HTTP struct {
		Addr string
	}
}

type Usecase struct {
	Timeout time.Duration
}

//IsDev GO_ENV == development
func IsDev() bool {
	return viper.Get(GoEnvKey) == DevEnv
}
