package model

type Response struct {
	Success bool        `json:"success"`
	Token   string      `json:"token"`
	Data    interface{} `json:"data,omitempty"`
	Error   string      `json:"error,omitempty"`
}
