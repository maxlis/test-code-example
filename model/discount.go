//go:generate easyjson -all
package model

import (
	"strconv"
	"time"
)

const layout = "02/01/2006"

type Discount struct {
	ID         uint32    `json:"id" pg:",pk,unique,notnull"`
	Name       string    `json:"name"`
	Phone      string    `json:"phone"`
	CardNum    string    `json:"card_num"`
	Bonus      float64   `json:"-"`
	Birthday   time.Time `json:"-"`
	LastUpdate time.Time `json:"-"`

	BonusDTO  string `pg:"-" json:"bonus"`
	BirthDTO  string `pg:"-" json:"birthday"`
	UpdateDTO string `pg:"-" json:"last_update"`
}

type Discounts []Discount

func (v *Discount) ToDTO() {
	if v.LastUpdate.IsZero() {
		v.LastUpdate = time.Now()
	}

	v.BonusDTO = strconv.FormatFloat(v.Bonus, 'f', -1, 32)
	v.BirthDTO = v.Birthday.Format(layout)
	v.UpdateDTO = v.LastUpdate.Format(layout)
}

func (v *Discount) FromDTO() {
	if v.LastUpdate.IsZero() {
		v.LastUpdate = time.Now()
	}

	v.Bonus, _ = strconv.ParseFloat(v.BonusDTO, 64)
	v.Birthday, _ = time.Parse(layout, v.BirthDTO)
}
