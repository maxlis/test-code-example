package model

const JWTSecret = "NocheMioAPISecret"

type JWTAuth struct {
	Phone string `json:"phone"`
	Token string `json:"token"`
}
