package repository

import (
	"context"
	"github.com/go-pg/pg/v9/orm"
)

type GoPG interface {
	Common
	Stop()
	Health(_ctx context.Context) error
}

type Common interface {
	Get(_ctx context.Context, in interface{}, hook ...func(*orm.Query) (*orm.Query, error)) (err error)
	GetWhere(_ctx context.Context, in interface{}, where string, param ...interface{}) error
	Insert(_ctx context.Context, in interface{}) (err error)
	Update(_ctx context.Context, in interface{}) (err error)
	UpdateWhere(_ctx context.Context, in interface{}, where string, param ...interface{}) (err error)
}
