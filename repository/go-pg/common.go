package gopg

import (
	"context"
	"github.com/go-pg/pg/v9/orm"
)

func (s *Client) Health(ctx context.Context) error {
	con := s.db.WithContext(ctx).Conn()

	defer func() {
		_ = con.Close()
	}()

	if _, err := con.ExecContext(ctx, "SELECT 1"); err != nil {
		return err
	}

	return nil
}

func (s *Client) Get(_ctx context.Context, in interface{}, hook ...func(*orm.Query) (*orm.Query, error)) (err error) {
	q := s.db.Model(in)

	for i := range hook {
		q = q.Apply(hook[i])
	}

	return q.Select()
}

func (s *Client) GetWhere(_ctx context.Context, in interface{}, where string, param ...interface{}) error {
	return s.db.Model(in).Where(where, param...).Select()
}

func (s *Client) Insert(_ctx context.Context, in interface{}) (err error) {
	return s.db.Insert(in)
}

func (s *Client) Update(_ctx context.Context, in interface{}) (err error) {
	_, err = s.db.Model(in).WherePK().Update()

	return err
}

func (s *Client) UpdateWhere(_ctx context.Context, in interface{}, where string, param ...interface{}) (err error) {
	_, err = s.db.Model(in).Where(where, param...).Update()

	return err
}
