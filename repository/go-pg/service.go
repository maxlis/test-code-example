package gopg

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"github.com/go-pg/pg/v9"
)

type Client struct {
	db *pg.DB
}

func New(sName string, cnf *Config, _ bool) *Client {
	var c *tls.Config

	if cnf.SSLMode != "disable" {
		c = &tls.Config{ServerName: cnf.Host}

		if cnf.SSLCert != "" && cnf.SSLKey != "" {
			cert, err := tls.LoadX509KeyPair(cnf.SSLCert, cnf.SSLKey)
			if err != nil {
				//TODO log here
			}

			c.Certificates = []tls.Certificate{cert}
		}

		if cnf.SSLRootCert != "" {
			CACert, err := ioutil.ReadFile(cnf.SSLRootCert)
			if err != nil {
				//TODO log here
			}

			CACertPool := x509.NewCertPool()
			CACertPool.AppendCertsFromPEM(CACert)

			c.RootCAs = CACertPool
		} else {
			c.InsecureSkipVerify = true
		}
	}

	db := pg.Connect(&pg.Options{
		User:            cnf.User,
		Password:        cnf.Password,
		Database:        cnf.DatabaseName,
		Addr:            cnf.Host + ":" + cnf.Port,
		ApplicationName: sName,
		MinIdleConns:    cnf.MaxIdleConns,
		PoolSize:        cnf.MaxOpenConns,
		TLSConfig:       c,
	})

	db.AddQueryHook(dbLogger{})

	res := &Client{db: db}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	if err := res.Health(ctx); err != nil {
		//TODO log here
	}

	return res
}

func (s *Client) Stop() {
	if err := s.db.Close(); err != nil {
		//TODO log here
	}
}

func (s *Client) DB() *pg.DB {
	return s.db
}

type dbLogger struct{}

func (d dbLogger) BeforeQuery(c context.Context, q *pg.QueryEvent) (context.Context, error) {
	return c, nil
}

func (d dbLogger) AfterQuery(c context.Context, q *pg.QueryEvent) error {
	fmt.Println(q.FormattedQuery())
	return nil
}

func E(err error) error {
	switch {
	case err == nil:
		return nil
	case err == pg.ErrNoRows:
		//TODO log here
	case strings.Contains(err.Error(), "duplicate"):
		//TODO log here
	}

	return err
}
