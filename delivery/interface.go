// Package delivery represent delivery interface relation
package delivery

type RouterInterface interface {
	Serve(addr string)
	Stop()
}
