package handlers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/max.lis/test-code-example/model"
	"io/ioutil"
	"time"
)

const apiImagesPath = "https://api.nochemio.com/images/"

//jwtCustomClaims are custom claims extending default ones.
type JWTClaims struct {
	Phone string `json:"phone"`
	jwt.StandardClaims
}

type Background struct {
	URL      string    `json:"url"`
	Name     string    `json:"-"`
	CreateAt time.Time `json:"-"`
}

//GET /health Service health
func (s *service) HealthHandler(ctx echo.Context) error {
	err := s.uc.Health(ctx.Request().Context())
	s.api.Logger.Debug(err)

	return ctx.JSON(ctx.Response().Status, err)
}

//JWT Auth
func (s *service) JWTAuth(ctx echo.Context) error {
	in := &model.JWTAuth{}
	if err := ctx.Bind(in); err != nil {
		s.api.Logger.Debug(err.Error())
		return ctx.JSON(echo.ErrUnauthorized.Code, model.Response{Success: false, Error: err.Error()})
	}

	res, err := s.uc.GetDiscountData(ctx.Request().Context(), in.Phone)
	if err != nil {
		s.api.Logger.Debug(err)
		return ctx.JSON(echo.ErrUnauthorized.Code, model.Response{Success: false, Error: err.Error()})
	}

	//Set custom claims
	claims := &JWTClaims{
		res.Phone,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}

	//Create token with claims
	t := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	//Generate encoded token and send it as response.
	token, err := t.SignedString([]byte(model.JWTSecret))
	if err != nil {
		s.api.Logger.Debug(err)
		return ctx.JSON(echo.ErrUnauthorized.Code, model.Response{Success: false, Error: err.Error()})
	}

	return ctx.JSON(ctx.Response().Status, model.Response{Success: true, Token: token, Data: res})
}

//GET /background
func (s *service) Background(ctx echo.Context) error {
	res := Background{}

	subDirs, err := ioutil.ReadDir("./static/images")
	if err != nil {
		s.api.Logger.Debug(err)
		return ctx.JSON(echo.ErrUnauthorized.Code, model.Response{Success: false, Error: err.Error()})
	}

	for _, v := range subDirs {
		if v.IsDir() && time.Now().Sub(v.ModTime()) < time.Now().Sub(res.CreateAt) {
			res.URL = apiImagesPath + v.Name() + "/"
			res.Name = v.Name()
			res.CreateAt = v.ModTime()
		}
	}

	return ctx.JSON(ctx.Response().Status, model.Response{Success: true, Data: res})
}
