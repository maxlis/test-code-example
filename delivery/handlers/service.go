package handlers

import (
	"context"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/max.lis/test-code-example/delivery"
	"gitlab.com/max.lis/test-code-example/model"
	"gitlab.com/max.lis/test-code-example/usecase"
)

type service struct {
	api *echo.Echo
	uc  usecase.Interface
}

func New(u usecase.Interface) delivery.RouterInterface {
	api := echo.New()

	//Static
	api.Use(middleware.Static("static"))

	//Middleware
	api.Use(middleware.Logger())
	api.Use(middleware.Recover())

	//CORS restricted
	api.Use(middleware.CORS())

	return &service{api: api, uc: u}
}

func (s *service) Serve(addr string) {
	s.api.GET("/health", s.HealthHandler)
	s.api.POST("/jwt-auth", s.JWTAuth)
	s.api.GET("/db-update", s.DiscountUpdate)
	s.api.GET("/background", s.Background)

	//handlers
	r := s.api.Group("/discount")

	//Configure middleware with the custom claims type
	config := middleware.JWTConfig{
		Claims:     &JWTClaims{},
		SigningKey: []byte(model.JWTSecret),
		ErrorHandlerWithContext: func(err error, ctx echo.Context) error {
			return ctx.JSON(echo.ErrUnauthorized.Code, model.Response{Success: false, Error: err.Error()})
		},
	}
	r.Use(middleware.JWTWithConfig(config))
	s.discountRoute(r)

	s.api.Logger.Fatal(s.api.Start(addr))
}

func (s *service) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), 1)
	defer cancel()

	if err := s.api.Shutdown(ctx); err != nil {
		s.api.Logger.Debug(err)
	}
}
