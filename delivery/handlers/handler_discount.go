package handlers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/max.lis/test-code-example/model"
)

func (s *service) discountRoute(g *echo.Group) {
	g.GET("/getdata", s.getDiscount)
}

func (s *service) getDiscount(ctx echo.Context) error {
	u := ctx.Get("user").(*jwt.Token)
	claims := u.Claims.(*JWTClaims)

	res, err := s.uc.GetDiscountData(ctx.Request().Context(), claims.Phone)
	if err != nil {
		s.api.Logger.Debug(err)
		return ctx.JSON(echo.ErrUnauthorized.Code, model.Response{Success: false, Error: err.Error()})
	}

	return ctx.JSON(ctx.Response().Status, model.Response{Success: true, Token: u.Raw, Data: res})
}

func (s *service) DiscountUpdate(ctx echo.Context) error {
	res, err := s.uc.UpdateDiscountData(ctx.Request().Context())
	if err != nil {
		s.api.Logger.Debug(err)
		return ctx.JSON(echo.ErrInternalServerError.Code, model.Response{Success: false, Error: err.Error()})
	}

	return ctx.JSON(ctx.Response().Status, model.Response{Success: true, Data: res})
}
